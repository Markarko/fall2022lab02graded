// Marko Litovchenko 2133905
package application;
import vehicules.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Trek", 8, 40);
        bicycles[1] = new Bicycle("Cannondale", 9, 44);
        bicycles[2] = new Bicycle("Giant", 5, 36);
        bicycles[3] = new Bicycle("Specialized", 10, 60);

        for (Bicycle bike : bicycles){
            System.out.println(bike);
        }
    }
}
