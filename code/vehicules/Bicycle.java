// Marko Litovchenko 2133905
package vehicules;

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    public String toString(){
        return String.format("Manufacturer: %s, Number of Gears: %d, MaxSpeed: %f", this.manufacturer, this.numberGears, this.maxSpeed);
    }
}